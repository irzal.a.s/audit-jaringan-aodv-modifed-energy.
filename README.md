modified AODV with energy features
.
.
Nama : Irzal ahmad S
NRP : 05111850010020

.Mt.Kul : TD. Desain Audit Jaringan
Improvement pada AODV standard dengan residual energy dengan menambahkan fitur seleksi energy yang dimiliki node.
Pada paper, energy setiap node pada rute AODV yang dipilih dijumlahkan. Rute dengan jumlah residual energy tertinggi dipilih untuk menjaga konektivitas pengiriman data.
Dilakukan penambahan seleksi yaitu menghindari rute yang memiliki node dengan residual energy terendah untuk menghindari putusnya koneksi.

Sehingga fokusan pada modifed aodv ini adalah 
1. Menentukan jalur terbaik berdasarkan energy.
2. Yang menentukan jalur tersebut adalah source bukan destination
3. Membolehkan 2 node source bertemu di 1 node.